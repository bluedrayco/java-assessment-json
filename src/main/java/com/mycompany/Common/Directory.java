/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Common;

import com.mycompany.Entity.Transaction;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author tecno
 */
public class Directory {

    static File createOrRetrieve(final String target) throws IOException {

        final Path path = Paths.get(target);

        if (Files.notExists(path)) {
            return Files.createFile(Files.createDirectories(path)).toFile();
        }
        return path.toFile();
    }

    public static boolean fileExists(final String target) throws IOException {
        final Path path = Paths.get(target);
        return Files.notExists(path);
    }

    public static void saveTransaction(String target, Transaction t) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        final Path path = Paths.get(target);
        if (Files.notExists(path)) {
            new File(path + "/" + t.getUser_id()).mkdirs();
        }
        PrintWriter writer = new PrintWriter(path + "/" + t.getUser_id() + "/" + t.getTransaction_id(), "UTF-8");
        writer.print(t);
        writer.close();
    }

    public static File[] getUserFiles(int userId) {
        File folder = new File("data/" + userId);
        File[] listOfFiles = folder.listFiles();
        return listOfFiles;
    }

    public static String getContentFromFile(File f) throws IOException {
        return Directory.getDataFromFile(f);
    }

    public static String getContentFromFile(String name) throws IOException {
        File f = new File(name);
        return Directory.getContentFromFile(f);
    }

    private static String getDataFromFile(File f) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(f);
        byte[] data = new byte[(int) f.length()];
        fis.read(data);
        fis.close();
        return (new String(data, "UTF-8"));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DAO;

import com.mycompany.Entity.Transaction;
import com.mycompany.Entity.TransactionSum;
import java.util.List;

/**
 *
 * @author tecno
 */
public interface TransactionDAO {
    public Transaction add(int userId,Transaction t);
    public Transaction show(int userId,String transactionId);
    public List<Transaction> list(int userId);
    public TransactionSum sum(int userId);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.DAO;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import com.google.gson.Gson;
import com.mycompany.Common.Directory;
import com.mycompany.Entity.Transaction;
import com.mycompany.Entity.TransactionSum;
import com.mycompany.SORT.SortByDate;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author tecno
 */
public class TransactionDAOImpl implements TransactionDAO {

    @Override
    public Transaction add(int userId, Transaction t) {
        TimeBasedGenerator gen = Generators.timeBasedGenerator(EthernetAddress.fromInterface());
        UUID uuid = gen.generate();
        t.setTransaction_id(uuid);
        t.setUser_id(userId);
        try {
            Directory.saveTransaction("data", t);
        } catch (IOException ex) {
            return null;
        }
        return t;
    }

    @Override
    public Transaction show(int userId, String transactionId) {
        File f = new File("data/" + userId + "/" + transactionId);
        Transaction t = null;
        try {
            t = transformToTransaction(f);
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
        return t;
    }

    @Override
    public List<Transaction> list(int userId) {
        try {
            List<Transaction> transactions = new ArrayList<>();
            File[] files = Directory.getUserFiles(userId);
            for (File f : files) {
                try {
                    transactions.add(transformToTransaction(f));
                    Collections.sort(transactions,new SortByDate());
                } catch (FileNotFoundException ex) {
                    return null;
                } catch (IOException ex) {
                    return null;
                }
            }
            return transactions;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public TransactionSum sum(int userId) {
        List<Transaction> transactions = this.list(userId);
        TransactionSum ts=new TransactionSum();
        ts.setSum(0.0);
        ts.setUser_id(userId);
        if(transactions==null){
            return ts;
        }
        for(Transaction t:transactions){
            ts.setSum(ts.getSum()+t.getAmount());
        }
        return ts;
    }

    private Transaction transformToTransaction(File f) throws UnsupportedEncodingException, FileNotFoundException, IOException {
        Gson gson = new Gson();
        String json = Directory.getContentFromFile(f);
        Transaction t = gson.fromJson(json, Transaction.class);
        return t;
    }
}

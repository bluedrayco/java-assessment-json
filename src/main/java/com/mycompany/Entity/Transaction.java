package com.mycompany.Entity;

import com.google.gson.Gson;
import java.util.UUID;

public class Transaction {
  private double amount = 0;
  private String description = "";
  private String date = "";
  private UUID transaction_id=UUID.randomUUID();
  private int user_id=0;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public UUID getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(UUID transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    
    public String toString(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
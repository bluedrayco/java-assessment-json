/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.SORT;

import com.mycompany.Entity.Transaction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class SortByDate implements Comparator<Transaction> {

    @Override
    public int compare(Transaction o1, Transaction o2) {
        SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date1;
        Date date2;
        try {
            date1 = dateParser.parse(o1.getDate());
            date2 = dateParser.parse(o2.getDate()); 
            if (date1.before(date2)) {
                return -1;
            } else if (date1.after(date2)) {
                return 1;
            } else {
                return 0;
            }
        } catch (ParseException ex) {
            Logger.getLogger(SortByDate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

}

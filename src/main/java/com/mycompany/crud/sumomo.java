package com.mycompany.crud;

import com.google.gson.Gson;
import com.mycompany.DAO.TransactionDAO;
import com.mycompany.DAO.TransactionDAOImpl;
import com.mycompany.Entity.Transaction;
import com.mycompany.Entity.TransactionSum;
import java.io.IOException;
import java.util.List;

public class sumomo {

    public static void main(String[] args) throws IOException {
        try {
            Gson gson = new Gson();
            String json = null;
            TransactionDAO tansactionDao = new TransactionDAOImpl();
            List<Transaction> transacciones=null;
            Transaction t ;
            TransactionSum s;
            switch (args[1]) {
                case "add":
                    t = tansactionDao.add(Integer.parseInt(args[0]), gson.fromJson(args[2], Transaction.class));
                    json = gson.toJson(t);
                    break;
                case "list":
                    transacciones = tansactionDao.list(Integer.parseInt(args[0]));
                    json = transacciones==null?"[]":gson.toJson(transacciones);
                    break;
                case "sum":
                    s=tansactionDao.sum(Integer.parseInt(args[0]));
                    json = gson.toJson(s);
                    break;
                default:
                    t = tansactionDao.show(Integer.parseInt(args[0]),args[1]);
                    json = t==null?"Transaction not found":gson.toJson(t);
                    break;
            }
            System.out.println(json);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("We need more arguments");
            System.exit(1);
        }
    }
}
